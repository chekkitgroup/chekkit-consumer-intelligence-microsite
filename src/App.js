import React, { useEffect } from 'react';
import { useDispatch } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import actions from "./actions";
import Home from './components/pages/home'
import Quiz from './components/pages/quiz'
import Finished from './components/pages/finished'
import Notifications from './components/partials/notifications';




const App = () => {
    const dispatch = useDispatch();
    
    useEffect(() =>{
        const params = new URLSearchParams(window.location.search);
        const slug = params.get('_k');
        dispatch(actions.fetchSurveyId(slug));

        dispatch(actions.fetchSurveysAPI(slug));
    },[dispatch]);
    
    return (
        <>
            <Notifications />
            <Router basename="/">
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/quiz" component={Quiz} />
                    <Route exact path="/finished" component={Finished} />
                </Switch>
            </Router>
        </>
    )
}

export default App;