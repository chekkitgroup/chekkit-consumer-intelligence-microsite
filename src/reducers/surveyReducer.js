import {FETCH_SURVEY_QUESTIONS, FETCH_SURVEY_ID} from '../actions/types';

import _ from "lodash";

export default (state ={}, action) => {
    switch (action.type) {
        case FETCH_SURVEY_QUESTIONS: {
            return {
              ...state,
              ...action.payload,
            };
          }
    
        default:
            return state;
    }
}