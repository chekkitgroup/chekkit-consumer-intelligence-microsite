import { SURVEY_SUBMIT, SURVEY_LOADING,SURVEY_COMPLETED, QUESTIONS_LOADING} from '../actions/types';

import _ from "lodash";

const initialState = {
  value: false
}
export default (state = initialState, action) => {
    switch (action.type) {
      case SURVEY_LOADING: 
        return {...state, value: true}

      case QUESTIONS_LOADING: 
        return {...state, qloadingvalue: true}
      
      case SURVEY_COMPLETED:
        return {...state, value: false}
      default:
          return state;
    }
}