import { FETCH_SURVEY_ID} from '../actions/types';

import _ from "lodash";

export default (state ={}, action) => {
    switch (action.type) {
        case FETCH_SURVEY_ID: {
          return {
            ...state,
            value: action.payload,
          };
        }
        default:
            return state;
    }
}