import {SIGNUP_SUBMITTED} from '../actions/types';

import _ from "lodash";

export default (state ={}, action) => {
    switch (action.type) {
        case SIGNUP_SUBMITTED: {
            return {
              ...state,
              ...action.payload,
            };
          }
    
        default:
            return state;
    }
}