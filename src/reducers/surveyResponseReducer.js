import { SUBMIT_SURVEY_RESPONSE} from '../actions/types';

import _ from "lodash";

export default (state ={}, action) => {
    switch (action.type) {
        case SUBMIT_SURVEY_RESPONSE: {
          return {
            ...state,
            value: action.payload,
          };
        }
        default:
            return state;
    }
}