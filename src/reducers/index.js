import {combineReducers} from 'redux';
import { reducer as formReducer } from "redux-form";
import surveyReducer from "./surveyReducer";
import surveyIdReducer from "./surveyIdReducer";
import loadingReducer from "./loadingReducer";
import surveyResponseReducer from "./surveyResponseReducer";
import signupReducer from "./signupReducer";

export default combineReducers({
    form: formReducer,
    survey: surveyReducer,
    surveyId: surveyIdReducer,
    loading:loadingReducer,
    surveyResponse:surveyResponseReducer,
    signup:signupReducer,
})
