const selectSurveyResponse = ( state ) => state.surveyResponse.value;

export default selectSurveyResponse;