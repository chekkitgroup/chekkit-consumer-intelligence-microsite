const selectLoadingQuestionsState = ( state ) => state.loading.values;

export default selectLoadingQuestionsState;