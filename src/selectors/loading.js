const selectLoadingState = ( state ) => state.loading.value;

export default selectLoadingState;