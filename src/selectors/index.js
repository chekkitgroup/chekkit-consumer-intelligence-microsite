import selectSurvey from "./survey";
import selectSurveyId from "./surveyId";
import selectLoadingState from "./loading";
import selectCampaign from "./campaign";
import selectSurveyResponse from "./surveyResponse";
import selectLoadingQuestionsState from "./loadingQuestions";
import selectSignupData from "./signup";

export default {
    selectSurvey,
    selectSurveyId,
    selectLoadingState,
    selectSignupData,
    selectCampaign,
    selectSurveyResponse,
    selectLoadingQuestionsState
}