import axios from "axios";


const BASE_URL = process.env.REACT_APP_BASE_URL;

export default axios.create({
    baseURL: BASE_URL,
    headers: {
      common: {
        token: 'ZCI6MiwidXNlcm5hbWUiOiJ2aWNzb2Z0IiwiaWF0IjoxNjI1NDM2NzMyLCJleHAiOjE2MjY2NDYzMzJ949P9973zRv8oo8GYD-0vm5VB6VDEFjixnRolEp4Nt8Q'
      }
    }
})