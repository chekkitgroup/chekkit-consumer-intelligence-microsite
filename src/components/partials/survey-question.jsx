import React from 'react';


const SurveyQuestion = ({choice, index, activeClass, clicked}) => {

    console.log("choice", index)
    return(
        <div className="lbl-item" onClick={clicked} >
            <span className={`lbl-brd ${activeClass}`}>{index}</span> <span className="lbl-txt">{choice.text}</span>
        </div>   
    )
}

export default SurveyQuestion;