import React from 'react';

import Logo from '../../assets/images/logo.png';

const Nav = () => {
    return (
        <div className="ui borderless menu transparent">
            <div className="ui container grid">
                <div className="row">
                    <img className="item" src={Logo} alt="Logo" />
                </div>
            </div>
        </div> 
    )
}

export default Nav;