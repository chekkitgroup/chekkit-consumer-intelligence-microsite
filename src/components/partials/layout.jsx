import React from 'react';
import Nav from './nav';
import Footer from './footer';
import PropTypes from 'prop-types';

const Layout = ({ children, className }) =>  (
    <>
        {/* <Nav /> */}
        {/*  */}
        <div className="page-wrapper">
            <div className="top-bg-section">
                <Nav />
            </div>
            <div className="bottom-bg-section"></div>
            <main className={`layout ${className}`}>
                <div className="ui container ">
                    {children}
                </div>
            </main>
        </div>
    </>
)

Layout.defaultProps = {
    className: ""
}

Layout.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string
}

export default Layout;