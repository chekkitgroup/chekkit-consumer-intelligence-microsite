import React, {useState, useEffect} from 'react';


const Card = ({children}) => {
    

    return (
        <div className="s-card">
            {children}
        </div>
    )
}

export default Card;