import React, {useState, useEffect} from 'react';
import { useDispatch, useSelector } from "react-redux";
import actions from '../../actions';
import selectors from '../../selectors';
import SurveyQuestion from './survey-question';
import Card from './card';
import useForceUpdate from 'use-force-update';
import {useHistory} from 'react-router-dom';



const parseChoices = (str) => JSON.parse(str);


const SurveyCard = ({children, survey, campaign}) => {
    const history = useHistory();

    console.log('campaign: ', campaign)
    const dispatch = useDispatch();
    const forceUpdate = useForceUpdate();

    const [currentQuestion, setCurrentQuestionIndex] = useState(1);

    const submiting = useSelector(selectors.selectLoadingState)
    const signup = useSelector(selectors.selectSignupData)
    const loadingQuestions = useSelector(selectors.selectLoadingQuestionsState)
    console.log('loadingQuestions: ', loadingQuestions)
    const surveyResponse = useSelector(selectors.selectSurveyResponse);

    const [responses, setResponses] = useState([]);
    const [activeClass, setActiveClass] = useState("");

    const isActiveQuestion = (n) =>  n === currentQuestion?'active-color':'';
    const isActive = (n) =>  n === currentQuestion?'active':'';
    
    const incrementQuestionIndex = () => ((responses.length > currentQuestion) && responses[currentQuestion-1]) && setCurrentQuestionIndex(currentQuestion+1);
    const decrementQuestionIndex = () => (currentQuestion > 1) &&  setCurrentQuestionIndex(currentQuestion-1);
    
    const answerSubmitted = (i, ans) => {
       let r =  responses.some(el => (el && el.choice === ans));
       if(r) setActiveClass('active-bg');
    };

    useEffect(() => {
        if(survey.questions){
            const arr = new Array(survey.questions.length).fill(null);
            setResponses(arr);
            console.log('7777 ', arr)
        }
      }, [survey.questions]);

    useEffect(() => {

        console.log(responses)
    }, [responses]);

    useEffect(() => {

    console.log('surveyResponse', surveyResponse)
    if(surveyResponse && surveyResponse.status){
        history.push(`/finished`);    
    }

    }, [surveyResponse]);


    const onAnswerSelected = (ans, i, id, cNum) => {
        console.log('onAnswerSelected', ans, i, id,cNum)
        // i = currentQuestion-1;
        // {id:q.id, choice:q.answer, surveyId:q.surveyId}
        let resObj = {choice:ans, q_num : i, q_id:id,cNum}
        let tempArr = responses;

        tempArr[(currentQuestion-1)]  = resObj;

        setResponses(tempArr)

        console.log(responses)
        incrementQuestionIndex()

        answerSubmitted(i,ans);
        forceUpdate();

        // console.log('survey: ',survey.questions[i].choices)
    }

    const submitSurvey = () => {
        let f = {
            surveyId : survey.survey.id,
            phone_number : signup.phone,
            campaignId : campaign.id,
            name: signup.name,
            phone: signup.phone,
            email: signup.email,
            network: signup.network,
            responses
        }
        // console.log(ifNullExists(responses))
        // dispatch(actions.submittingState(true));
        if(ifNullExists(responses)){
            alert('please answer all questions');
            return false;
        }
        dispatch(actions.submitSurveyAPI(f));

        // console.log(f);

    }

    const ifNullExists = (arr) => {
        return arr.some(function (el) {
            return el === null;
        });
    }
    return (
        <div>
            {survey.questions && survey.questions.length > 0 && survey.questions.map((d, index) => (
            <div>
                    {currentQuestion === (index+1) && (
                        <Card className="s-card" key={d.id}>
                            <div>
                                <div className="carousel-controls">
                                    <div className="control-item arrow-container" onClick={() => decrementQuestionIndex()}>
                                        <i className={`arrow alternate circle left icon`}></i>
                                    </div>
                                    <div className="control-item index-txt"><span>{`0${index+1}/0${survey.questions.length}`}</span></div>
                                    <div className="control-item arrow-container" onClick={() => incrementQuestionIndex()}>
                                        <i className="arrow alternate circle right icon"></i>
                                    </div>                    
                                </div>
                                <div className="carousel-indicators">
                                    {survey.questions.map((d, n) => (
                                        <div key={n} className={`indicator-item ${isActive((1+n))}`}></div>   
                                    ))}
                                </div>
                            </div>
                            <div className="row question-text-wrapper">
                                <div className="question-text-container">
                                    <p>{d.content}</p>
                                </div>
                                <div>
                                    {parseChoices(d.choices).map((choice, index2) => (
                                        <div>
                                            <SurveyQuestion activeClass={(responses[index] && responses[index].cNum == (index2))?activeClass:'' } clicked={() => onAnswerSelected(choice.text, (index+1), d.id, index2)} key={index2} choice={choice} choiceIndex ={index2} index={index2 + 1}/>
                                        </div>
                                    ))}

                                        <p>{loadingQuestions}</p>
                                </div>            
                            </div>
                            <div className="row">
                                {currentQuestion == survey.questions.length &&  (
                                    <div>
                                        <button onClick={() => submitSurvey()} className={`ui button ${submiting?'loading':''} blue_bg right floated`}>
                                            Submit Survey
                                        </button>
                                    </div>
                                )}   
                            </div> 
                        </Card>
                    )}
                </div>

            ))}

            {!survey.questions  && (
                <Card className="s-card">
                    <div class="ui segment spinner-segment">
                        <div class="ui active inverted dimmer">
                            <div class="ui text loader">loading questions...</div>
                        </div>
                        <p></p>
                    </div>
                </Card>
            )}
        </div>
    )
}

export default SurveyCard;