import React from 'react';
import Nav from './nav';
import PropTypes from 'prop-types';

const FullLayout = ({ children, className }) =>  (
    <>
        <div className="page-wrapper">
            <div className={`full-bg ${className}`}>
                <Nav />
                <main className={`full-layout`}>
                    <div className="ui container ">
                        {children}
                    </div>
                </main>
            </div>
        </div>
    </>
)

FullLayout.defaultProps = {
    className: ""
}

FullLayout.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string
}

export default FullLayout;