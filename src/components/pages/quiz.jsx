import React from 'react';
import { useSelector } from "react-redux";

import Layout from '../partials/layout'
import SurveyCard from '../partials/survey-card';
import selectors from "../../selectors";

const Quiz = () => {
    const survey = useSelector(selectors.selectSurvey)
    const campaign = useSelector(selectors.selectCampaign)

    console.log(survey)
    return (
        <Layout>
            <section>
                <h3 className="quiz-header">Take a quick Survey and get instant rewards</h3>
                <div className="">
                    <SurveyCard survey={survey} campaign={campaign}>
                    </SurveyCard>
                </div>
            </section>
        </Layout>
    )
}

export default Quiz;