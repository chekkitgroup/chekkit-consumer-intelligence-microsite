import React from 'react';
import {useHistory} from 'react-router-dom';
import { useSelector } from "react-redux";

import FullLayout from '../partials/full-layout'
import selectors from "../../selectors";
import Card from '../partials/card';
import Cash from '../../assets/images/cash.png';

const Finished = () => {
    const history = useHistory();

    const takeQuiz = (slug) => {
        history.push(`/quiz?_k=${surveyId}`);
    };
    const surveyId = useSelector(selectors.selectSurveyId)
    const surveyResponse = useSelector(selectors.selectSurveyResponse)

    console.log(surveyId)
    return (
        <FullLayout className="bg2">
            <Card>
                <section className="success-message-section">
                    <div className="success-div-left">
                        <img className="cash-img" src={Cash} alt="Logo" />
                    </div>
                    <div className="success-div-right">
                        <p className="success-p">
                        Congratulations!!! you have succesfully take the survey 
                        {/* Congratulations!!! you have won N{surveyResponse?surveyResponse.data.reward_value:''} reward  */}
                        </p>
                        <div className="ui center aligned ">
                            <button onClick={() => takeQuiz()} className="ui button blue_bg  floated">
                                <i className="icon share alternate"></i>
                                share on social media
                            </button>
                        </div>
                    </div>
                </section>
            </Card>
        </FullLayout>
    )
}

export default Finished;