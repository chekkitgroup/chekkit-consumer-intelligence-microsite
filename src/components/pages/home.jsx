import React, {  useState, useEffect } from 'react'
import {useHistory} from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";

import FullLayout from '../partials/full-layout'
import selectors from "../../selectors";
import actions from '../../actions';

const Home = () => {
    const history = useHistory();
    const dispatch = useDispatch();

    const takeQuiz = () => {
        history.push(`/quiz?_k=${surveyId}`);
    };
        
    const [formData, setFormData] = useState({
        name: '',
        phone: '',
        network: '',
        email: '',
    });


    const changeHandler = e => {
        console.log(e)
        setFormData({...formData, [e.target.name]: e.target.value})
    }

    const surveyId = useSelector(selectors.selectSurveyId)
    const survey = useSelector(selectors.selectSurvey)
    // console.log(survey)



    const handleSubmit= () => {

        console.log(formData)
        dispatch(actions.submitSignupData(formData));
        takeQuiz()
    }


    return (
        <FullLayout className="bg1">
            <section className="info-section">
                <h3 className="home-title">Hello, Welcome to Unilever Omo Campaign</h3>
                <p className="home-p">Take a quick survey and get instant rewards. Please click the button below to start survey</p>
                <div className="reg-form">
                    <h4>Please fill in the following details to take survey</h4>

                    
                <div className="ui form">
                    <div className="field">
                        <label> Full Name</label>
                        <input 
                            type="text"
                            placeholder="Full Name"
                            className="reg-input"
                            name="name"
                            value={formData.name}
                            onChange={changeHandler}
                        />
                    </div>

                    <div className="field">
                        <label className="label">Network</label>
                            <select class="custom-select mt-3 reg-input" name="network" onChange={changeHandler}>
                                <option value="1">Cell C</option>
                                <option value="2">MTN</option>
                                <option value="3">Telkom</option>
                                <option value="4">Vodacom</option>
                            </select>
                    </div>

                    <div className="field">
                        <label>Phone Number</label>
                        <input 
                            placeholder="Phone Number"
                            className="reg-input"
                            name="phone"
                            type="tel"
                            value={formData.phone}
                            onChange={changeHandler}
                        />
                    </div>

                    <div className="field">
                        <label>Email Address (optional)</label>
                        <input 
                            placeholder="Email Address"
                            className="reg-input"
                            name="email"
                            type="email"
                            value={formData.email}
                            onChange={changeHandler}
                        />
                    </div>
                </div> <br/>
                
                <div className="ui center aligned ">
                    <button disabled={survey && !survey.questions} onClick={() => handleSubmit()} className="ui button green_bg  floated">
                        Start Survey

                        {survey && !survey.questions  && (
                            <div className="ui active inverted dimmer">
                                <div className="ui text loader">loading questions...</div>
                            </div>
                        )}
                    </button>
                </div>
                </div>
            </section>
        </FullLayout>
    )
}

export default Home;