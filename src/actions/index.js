import {fetchSurveysAPI,submitSurveyAPI,fetchSurveyId} from "./survey";
import {submitSignupData} from "./signup";

export default {
    fetchSurveysAPI,
    submitSurveyAPI,
    fetchSurveyId,
    submitSignupData
}