import {FETCH_SURVEY_QUESTIONS, SUBMIT_SURVEY_RESPONSE, FETCH_SURVEY_ID, QUESTIONS_LOADING, SURVEY_LOADING, SURVEY_COMPLETED} from './types';
import surveys from '../apis/surveys'
import { toast } from 'react-toastify';

// import {useHistory} from 'react-router-dom';
// import {useNavigate} from 'react-router-dom';


// const history = useHistory();
// const navigate = useNavigate();

export const fetchSurveysAPI = (slug) => async dispatch =>{
    dispatch({type:QUESTIONS_LOADING, payload: true}) 

    try {
        const response = await surveys.get(`/web-qrcode-campaign/${slug}`) 
        // .then((response) => response.json())
        .then((response) => {
                console.log(response)
                return response;
        })
        .catch((error) => {
            console.log(error)
        //   toast.error(`Error: ${error.message}`);
        });

        console.log(response)

        dispatch({type:FETCH_SURVEY_QUESTIONS, payload: response.data.data}) 
        dispatch({type:QUESTIONS_LOADING, payload: false}) 

    }catch (error) {
        // console.log(error)
        toast.error(`Error: Could not load survey`);
    }
}


// export const fetchSurveysAPI = (slug) => async dispatch =>{
//     const response = await surveys.get(`/web-qrcode-campaign/${slug}`) 
//     dispatch({type:FETCH_SURVEY_QUESTIONS, payload: response.data.data})
// }

export const fetchSurveyId = (slug) => async dispatch =>{
    dispatch({type:FETCH_SURVEY_ID, payload: slug})
}

export const submitSurveyAPI = formValues => async (dispatch) =>{
   try {
    dispatch({type: SURVEY_LOADING});

    const response = await surveys.post('/qrcode-survey-response', {...formValues})
    
    // dispatch({ type: SUBMIT_SURVEY_RESPONSE, payload: {hh:888} })
    dispatch({ type: SUBMIT_SURVEY_RESPONSE, payload: response.data })
    dispatch({type: SURVEY_COMPLETED});

    // if(response && response.data && response.data.data.status){
    //     history.push(`/finished`);
    //     navigate('/finished');

    // }

    console.log('response: ',response)
   } catch (error) {
    dispatch({type: SURVEY_COMPLETED});
    toast.error(`Error: Could not submit survey`);
   }
};


export default fetchSurveysAPI;