import {SIGNUP_SUBMITTED} from './types';

export const submitSignupData = (data) => async dispatch =>{
    dispatch({type:SIGNUP_SUBMITTED, payload: data})
}


export default submitSignupData;